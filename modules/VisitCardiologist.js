import { sentRequest } from "../functions/sendRequest.js";
import { Visit } from "./Visit.js";
import { token } from "../confiqs/token.js";
import { API } from "../confiqs/API.js";
import { CardCardiologist } from "./CardCardiologist.js";

export class VisitCardiologist extends Visit {
    renderSpecialInfo() {
        const specialInfo = document.createElement("div");
        specialInfo.className = "special-info";
        specialInfo.insertAdjacentHTML("beforeend", `
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Normal pressure</p>
                    <input id="pressure" class="form-control" type='text' name="pressure" placeholder="Normal pressure">
                </label>
        </div>
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Body mass index</p>
                    <input id="massIndex" class="form-control" type='text' name="massIndex" placeholder="Body mass index">
                </label>
        </div>
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Transferred diseases of the cardiovascular system</p>
                    <input id="diseases" class="form-control" type='text' name="diseases" placeholder="Transferred diseases">
                </label>
        </div>
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Age</p>
                    <input id="age" class="form-control" type='text' name="age" placeholder="Age">
                </label>
        </div>
        <button type="submit" class="btn btn-primary">Send</button>
        `)
        return specialInfo;
    }
    render(container) {
        const doc = document.createElement("div");
        doc.className = "cardiologist doc";
        doc.append(this.renderGeneralInfo(), this.renderSpecialInfo());
        container.append(doc);
        const form = document.querySelector(".doctor-choose")
        this.sendDentistData(form, container);
        return doc;
    }

    sendDentistData(form, container) {
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            const [purpose, description, fullname, pressure, bodymass, desiases, age] = event.target.querySelectorAll("[type='text']");
            const [doctor, urgency] = event.target.querySelectorAll("select");
            const cardDesk = document.querySelector(".card-desk");
            const modal = document.querySelector(".modal");
            const bg = document.querySelector(".modal-backdrop");
            console.log(purpose.value, description.value, fullname.value, doctor.value, urgency.value, age.value);
            if (purpose !== '') {
                sentRequest(`${API}cards`, "POST" ,{
                    // method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        doctor: doctor.value,
                        description: description.value,
                        fullname: fullname.value,
                        urgency: urgency.value,
                        purpose: purpose.value,
                        pressure: pressure.value,
                        bodymass: bodymass.value,
                        desiases: desiases.value,
                        age: age.value,
                    })

                }).then((data) => {
                    console.log(data);
                    if (document.querySelector(".no-items")) {
                        document.querySelector(".no-items").remove()
                    }
                    if (data) {
                        const userCard = new CardCardiologist(data);
                        userCard.renderCard(cardDesk);
                        modal.remove();
                        bg.remove();
                        document.body.classList.remove("modal-open");
                    }
                })
            }
        })
    }
}