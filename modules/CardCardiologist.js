import { Card } from "./Card.js";

export class CardCardiologist extends Card {
    constructor ({id, doctor, purpose, description, urgency, fullname, lastvisit, pressure, bodymass, desiases, age}) {
        super({id, doctor, purpose, description, urgency, fullname})
        this.lastvisit = lastvisit;
        this.bodymass = bodymass;
        this.desiases = desiases;
        this.age = age;
        this.pressure = pressure;
    }

    renderSpecialInfo() {
        const specialList = document.createElement("ul");
        specialList.className = "list-group list-group-flush";
        specialList.insertAdjacentHTML("beforeend", `
        <li class="list-group-item">Normal pressure: ${this.pressure}</li>
        <li class="list-group-item">Body mass index: ${this.bodymass}</li>
        <li class="list-group-item">Transferred diseases of the cardiovascular system: ${this.desiases}</li>
        <li class="list-group-item">Age: ${this.age}</li>
        <li class="list-group-item"><a href="#" class="btn btn-primary btn_edit">Edit</a></li>
        `)
        return specialList;
    }

     renderCard(container) {
        const card = document.createElement("div");
        card.className = "card";
        card.dataset.userId = this.id;
        card.append(this.renderHeader(), this.renderMore(), this.renderSpecialInfo());
        container.append(card);
        return card;
    }
}