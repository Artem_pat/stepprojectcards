import { API } from "../confiqs/API.js";
import { token } from "../confiqs/token.js";
import { sentRequest } from "../functions/sendRequest.js";
import { Card } from "./Card.js";
import { CardCardiologist } from "./CardCardiologist.js";
import { CardDentist } from "./CardDentist.js";
import { CardTherapist } from "./CardTherapist.js";
import { mainArr } from "../main.js";
export class EnterUserForm {
    render() {
        this.formUser = document.createElement("form");
        this.formUser.insertAdjacentHTML("afterbegin", `
            <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>email</p>
                    <input id="email" class="form-control" type='text' name="fullname" placeholder="Email">
                </label>
            </div>
            <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>password</p>
                    <input id="password" class="form-control" type='password' name="password" placeholder="password">
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Enter</button>
        `)
        this.sendUserData(this.formUser);
        return this.formUser;
    }

    sendUserData(form) {
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            const enterModal = document.querySelector(".modal");
            const background = document.querySelector(".modal-backdrop");
            const btnEnter = document.querySelector(".btn_enter");
            const btnCreate = document.querySelector(".btn_create_visit");
            const cardDesk = document.querySelector(".card-desk");
            const noItems = document.createElement("p");
            noItems.className = "no-items";
            noItems.textContent = "No items have been added";
            cardDesk.append(noItems);
            const email = event.target.querySelector("#email").value;
            const password = event.target.querySelector("#password").value;

            if (email == "q" && password === "q") {
                sentRequest(`${API}cards`, "GET", {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).then((users) => {
                    mainArr.splice(0, 0, ...users)
                    console.log(mainArr);
                    console.log(users);
                    enterModal.remove();
                    background.remove();
                    document.body.classList.remove("modal-open");
                    btnEnter.style.display = "none";
                    btnCreate.style.display = "block";

                    if (users.length > 0) {
                        noItems.remove();
                        users.forEach(user => {
                            console.log(user.doctor);
                            
                            if (user.doctor === "dentist") {
                                const userCard = new CardDentist(user);
                                userCard.renderCard(cardDesk);
                            }
                            if (user.doctor === "therapist") {
                                const userCard = new CardTherapist(user);
                                userCard.renderCard(cardDesk);
                            }
                            if (user.doctor === "cardiologist") {
                                const userCard = new CardCardiologist(user);
                                userCard.renderCard(cardDesk);
                            }
                        });
                    } else {
                        cardDesk.append(noItems);
                    }
                })

            } else {
                alert("Error!!!")
            }

        })
    }


}