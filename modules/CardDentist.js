import { Card } from "./Card.js";

export class CardDentist extends Card {
    constructor ({id, doctor, purpose, description, urgency, fullname, lastvisit}) {
        super({id, doctor, purpose, description, urgency, fullname})
        this.lastvisit = lastvisit;
    }

    renderSpecialInfo() {
        this.specialList = document.createElement("ul");
        this.specialList.className = "list-group list-group-flush";
        this.specialList.style.display = "none";
        this.specialList.insertAdjacentHTML("beforeend", `
        <li class="list-group-item">Date of last visit: ${this.lastvisit}</li>
        <li class="list-group-item"><a href="#" class="btn btn-primary btn_edit">Edit</a></li>
        `)

        return this.specialList;
    }

     renderCard(container) {
        const card = document.createElement("div");
        card.className = "card";
        card.dataset.userId = this.id;
        card.append(this.renderHeader(), this.renderMore(), this.renderSpecialInfo());
        console.log(card);
        container.append(card);
        // return card;
    }
}