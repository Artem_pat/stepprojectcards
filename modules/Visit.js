
export class Visit {
    renderDoctor() {
        this.doctor = document.createElement("form");
        this.doctor.className = "doctor-choose"
        this.doctor.insertAdjacentHTML("beforeend", `
            <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Choose a doctor</p>
                    <select id="doctor" class="form-select" aria-label="Default select example">
                    <option selected>Choose a doctor</option>
                    <option value="dentist">Dentist</option>
                    <option value="cardiologist">Cardiologist</option>
                    <option value="therapist">Therapist</option>
                    </select>
                </label>
            </div>
            <div class = "doc-form">
            </div>
        `)
        return this.doctor;
    }

    renderGeneralInfo() {
        this.generalInfo = document.createElement("div");
        this.generalInfo.className = "general-info";
        this.generalInfo.insertAdjacentHTML("beforeend", `
        <div class="form-group">
            <label class="form-label" style="width: 100%;">
                    <p>The purpose of the visit</p>
                    <input id="purposeVisit" class="form-control" type='text' name="purposeVisit" placeholder="the purpose of the visit">
                </label>
        </div>
        <div class="form-group">
            <label class="form-label" style="width: 100%;">
                    <p>A brief description of the visit</p>
                    <input id="desrVisit" class="form-control" type='text' name="desrVisit" placeholder="a brief description of the visit">
                </label>
        </div>
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Urgency</p>
                    <select class="form-select" aria-label="Default select example">
                    <option selected>urgency</option>
                    <option value="normal">Noraml</option>
                    <option value="high">High</option>
                    <option value="low">Low</option>
                    </select>
                </label>
            </div>
        <div class="form-group">
            <label class="form-label" style="width: 100%;">
                    <p>Full name</p>
                    <input id="fullname" class="form-control" type='text' name="fullname" placeholder="fullname">
                </label>
        </div>
        `)
        return this.generalInfo;
    }

}