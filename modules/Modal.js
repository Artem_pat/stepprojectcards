export class Modal {
    constructor(title, body, closeOutside) {
        this.title = title;
        this.body = body;
        this.closeOutside = closeOutside;
    }

    render() {
        this.newModal = document.createElement("div");
        this.newModal.className = "modal";
        this.newModal.style.display = "block";
        document.body.classList.add("modal-open");

        this.newModal.insertAdjacentHTML("beforeend", `
        <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">${this.title}</h5>
            <button type="button" class="btn-close"></button>
          </div>
          <div class="modal-body">
          
          </div>
         </div>
        </div>
        `)

        if (this.body) {
            this.newModal.querySelector(".modal-body").append(this.body)
        }

        this.renderBackround();
        this.attachListener();
        return this.newModal;
    }
    
    closeModal() {
        this.newModal.remove();
        document.body.classList.remove("modal-open");
        this.background.remove();
    }

    renderBackround() {
        this.background = document.createElement("div");
        this.background.classList.add("modal-backdrop");
        document.body.append(this.background);
    }

    attachListener() {
        document.body.addEventListener("click", (event) => {
            const modal = event.target.classList.contains('modal');
            const close = event.target.classList.contains("btn-close");
            
            if (modal && this.closeOutside || close) {
                this.closeModal()
            }
        })

        return document.body.removeEventListener("click", (event) => {
            const modal = event.target.classList.contains('.modal');
            const close = event.target.classList.contains(".btn-close");
            if (modal && this.closeOutside || close) {
                this.closeModal()
            }
        })
    }
}