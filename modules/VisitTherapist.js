import { sentRequest } from "../functions/sendRequest.js";
import { Visit } from "./Visit.js";
import { token } from "../confiqs/token.js";
import { API } from "../confiqs/API.js";
import { CardTherapist } from "./CardTherapist.js";

export class VisitTherapist extends Visit {
    renderSpecialInfo() {
        const specialInfo = document.createElement("div");
        specialInfo.className = "special-info";
        specialInfo.insertAdjacentHTML("beforeend", `
         <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Age</p>
                    <input id="age" class="form-control" type='text' name="age" placeholder="Age">
                </label>
        </div>
        <button type="submit" class="btn btn-primary">Send</button>
        `)
        return specialInfo;
    }

    render(container) {
        const doc = document.createElement("div");
        doc.className = "tharapist doc";
        doc.append(this.renderGeneralInfo(), this.renderSpecialInfo());
        container.append(doc);
        const form = document.querySelector(".doctor-choose")
        this.sendDentistData(form, container);
        return doc;
    }

    sendDentistData(form, container) {
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            const [purpose, description, fullname, age] = event.target.querySelectorAll("[type='text']");
            const [doctor, urgency] = event.target.querySelectorAll("select");
            const cardDesk = document.querySelector(".card-desk");
            const modal = document.querySelector(".modal");
            const bg = document.querySelector(".modal-backdrop");
            console.log(purpose.value, description.value, fullname.value, age.value, doctor.value, urgency.value);
            if (purpose !== '') {
                sentRequest(`${API}cards`, "POST" ,{
                    // method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        doctor: doctor.value,
                        description: description.value,
                        fullname: fullname.value,
                        age: age.value,
                        urgency: urgency.value,
                        purpose: purpose.value,
                    })

                }).then((data) => {
                    console.log(data);
                    if (document.querySelector(".no-items")) {
                        document.querySelector(".no-items").remove()
                    }
                    if (data) {
                        const userCard = new CardTherapist(data);
                        userCard.renderCard(cardDesk);
                        modal.remove();
                        bg.remove();
                        document.body.classList.remove("modal-open");
                    }
                })
            }
        })
    }
}