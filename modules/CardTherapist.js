import { Card } from "./Card.js";

export class CardTherapist extends Card {
    constructor ({id, doctor, purpose, description, urgency, fullname, age}) {
        super({id, doctor, purpose, description, urgency, fullname})
        this.age = age;
    }

    renderSpecialInfo() {
        const specialList = document.createElement("ul");
        specialList.className = "list-group list-group-flush";
        specialList.insertAdjacentHTML("beforeend", `
        <li class="list-group-item">Date of last visit: ${this.age}</li>
        <li class="list-group-item"><a href="#" class="btn btn-primary btn_edit">Edit</a></li>
        `)

        return specialList;
    }

     renderCard(container) {
        const card = document.createElement("div");
        card.className = "card";
        card.dataset.userId = this.id;
        card.append(this.renderHeader(), this.renderMore(), this.renderSpecialInfo());
        container.append(card);
        return card;
    }
}