import { sentRequest } from "../functions/sendRequest.js";
import { Visit } from "./Visit.js";
import { token } from "../confiqs/token.js";
import { API } from "../confiqs/API.js";
import { CardDentist } from "./CardDentist.js";

export class VisitDentist extends Visit {
    renderSpecialInfo() {
        this.specialInfo = document.createElement("div");
        this.specialInfo.className = "special-info";
        this.specialInfo.insertAdjacentHTML("beforeend", `
        <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Date of last visit</p>
                    <input id="lastVisitDate" class="form-control" type='text' name="lastVisitDate" placeholder="Date of last visit">
                </label>
        </div>
        <button type="submit" class="btn btn-primary">Send</button>
        `)
        return this.specialInfo;
    }
    render(container) {
        this.doc = document.createElement("div");
        this.doc.className = "dentist doc"
        this.doc.append(this.renderGeneralInfo(), this.renderSpecialInfo());
        container.append(this.doc);
        const form = document.querySelector(".doctor-choose")
        this.sendDentistData(form, container);
        return this.doc;
    }
    sendDentistData(form, container) {
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            const [purpose, description, fullname, lastvisit] = event.target.querySelectorAll("[type='text']");
            const [doctor, urgency] = event.target.querySelectorAll("select");
            const cardDesk = document.querySelector(".card-desk");
            const modal = document.querySelector(".modal");
            const bg = document.querySelector(".modal-backdrop");
            console.log(purpose.value, description.value, fullname.value, lastvisit.value, doctor.value, urgency.value);
            if (purpose !== '') {
                sentRequest(`${API}cards`, "POST" ,{
                    // method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        doctor: doctor.value,
                        description: description.value,
                        fullname: fullname.value,
                        lastvisit: lastvisit.value,
                        urgency: urgency.value,
                        purpose: purpose.value,
                    })

                }).then((data) => {
                    console.log(data);
                    if (document.querySelector(".no-items")) {
                        document.querySelector(".no-items").remove()
                    }
                    if (data) {
                        const userCard = new CardDentist(data);
                        userCard.renderCard(cardDesk);
                        modal.remove();
                        bg.remove();
                        document.body.classList.remove("modal-open");
                    }
                })
            }
        })
    }
}