import { sentRequest } from "../functions/sendRequest.js";
import { token } from "../confiqs/token.js";
import { API } from "../confiqs/API.js";

export class Card {
    constructor({ id, doctor, purpose, description, urgency, fullname }) {
        this.id = id;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.fullname = fullname;
        this.urgency = urgency;
    }

    renderHeader() {
        const bodyCard = document.createElement("div");
        bodyCard.classList.add("card-body");
        bodyCard.insertAdjacentHTML("beforeend", `
        <div style=" display: flex; align-items: center; justify-content: space-between; ">
            <h5 class="card-title">Full name:  ${this.fullname}</h5>
            <button type="button" class="btn-close" aria-label="Close"></button>
        </div>
            <p class="card-text">Doctor: ${this.doctor}</p>
            <a href="#" class="btn btn-primary btn-show-more">Show more</a>
        `)

        // const showMore = document.querySelector(".btn-show-more");
        // console.log(showMore);
        // showMore.addEventListener("click", () => {
        //     this.specialList.style.display = "block";
        //     this.bodyList.style.display = "block";
        // })


        bodyCard.addEventListener("click", (event) => {
            console.log(event.target);
            const cardDesk = document.getElementsByClassName("card-desk");
            const card = document.querySelector(".card")
            const del = event.target.closest(".btn-close");
            const showMore = event.target.closest(".btn-show-more");
            console.log(showMore);
            if (del) {
                sentRequest(`${API}cards/${this.id}`, "DELETE", {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).then((data) => {
                    if (data.ok) {
                        card.remove();
                    }
                    if (cardDesk[0].children.length < 1) {
                        const noItems = document.createElement("p");
                        noItems.className = "no-items";
                        noItems.textContent = "No items have been added";
                        cardDesk[0].append(noItems);
                    }
                })
            }
            if (showMore) {
                this.bodyList.style.display = "block";
                this.specialList.style.display = "block";
            }
        })

        return bodyCard;
    }

    renderMore() {
        this.bodyList = document.createElement("ul");
        this.bodyList.className = "list-group list-group-flush";
        this.bodyList.style.display = "none";
        this.bodyList.insertAdjacentHTML("beforeend", `
        <li class="list-group-item">The purpose of the visit: ${this.purpose}</li>
        <li class="list-group-item">A brief description of the visit: ${this.description}</li>
        <li class="list-group-item">Urgency: ${this.urgency}</li>

        `)
        return this.bodyList;
    }

    // renderCard(container) {
    //     const card = document.createElement("div");
    //     card.className = "card";
    //     card.dataset.userId = this.id;
    //     card.append(this.renderHaeder(), this.renderMore());
    //     container.append(card);
    //     return card;
    // }
}