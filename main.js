import { API } from "./confiqs/API.js";
import { token } from "./confiqs/token.js";
import { sentRequest } from "./functions/sendRequest.js";
import { Visit } from "./modules/Visit.js";
import { Modal } from "./modules/Modal.js";
import { EnterUserForm } from "./modules/EnterUserForm.js";
import { VisitDentist } from "./modules/VisitDentist.js";
import { VisitCardiologist } from "./modules/VisitCardiologist.js";
import { VisitTherapist } from "./modules/VisitTherapist.js";

const btnEnter = document.querySelector(".btn_enter");
const btnCreate = document.querySelector(".btn_create_visit");
export let mainArr = [];
btnEnter.addEventListener("click", () => {
    const newUserForm = new EnterUserForm();
    const newUserModal = new Modal("New User", newUserForm.render(), true);
    document.body.append(newUserModal.render());
})

btnCreate.addEventListener("click", () => {
    const newVisitForm = new Visit();
    const newVisitModl = new Modal("New Visit", newVisitForm.renderDoctor(), true);
    document.body.append(newVisitModl.render());
    const doctors = document.querySelector("#doctor");

    doctors.addEventListener("change", (event) => {
        let doctor = event.target.value;
        console.log(doctor);
        const formDoc = document.querySelector(".doc-form");
        console.log(formDoc);
        if (doctor === "dentist") {
            formDoc.innerHTML = "";
            const newForm = new VisitDentist();
            newForm.render(formDoc);
        }
        if (doctor === "cardiologist") {
            formDoc.innerHTML = "";
            const newForm = new VisitCardiologist();
            newForm.render(formDoc);
        }
        if (doctor === "therapist") {
            formDoc.innerHTML = "";
            const newForm = new VisitTherapist();
            newForm.render(formDoc);
        }
    })
})