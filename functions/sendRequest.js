export async function sentRequest(url, method = "GET", config) {
    const request = await fetch(url, {method: method, ...config});
    if (method === "DELETE") {
        return request
    } else {
        return request.json();
    }
}
